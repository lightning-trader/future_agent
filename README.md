# future_agent

#### 介绍
一个基于强化学习的期货高频择时策略，采用离线tick数据训练，
神经网络引擎使用的是pytorch；
强化学习引擎使用的是stable baseline3；
实盘交易使用的是ctpbee；
算法支持ppo，rppo，sac，td3；
支持自动调参

#### 软件架构
软件架构说明


#### 安装教程

1.  首先需要安装pytorch环境，可以选择cpu或者gpu环境
2.  安装stable baseline3
3.  修改DataCenter数据路径，修改TRAINING_BEGIN_TIME要训练的日tick数据
4.  选择需要的算法训练
5.  实盘需要开通期货公司外接获得appid和auth_code账户密码等信息，具体询问期货公司客户经理
6.  实盘用户需要修改run_sac.py修改账户信息启动即可，其他算法可以参考run_sac.py

#### 使用说明

QQ交流群 : 768621846

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
