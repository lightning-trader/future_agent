
from stable_baselines3.common.monitor import Monitor
from stable_baselines3.common.utils import set_random_seed
from stable_baselines3 import TD3
from training_env import TrainingEnv
from save_model import SaveModelCallback
from stable_baselines3.common.vec_env import SubprocVecEnv
from stable_baselines3.common.noise import NormalActionNoise
import torch as th
import numpy as np
from stable_baselines3.common.evaluation import evaluate_policy

TB_LOG_PATH = "../tb_log"
MODEL_PATH = "./model/td3"

LEARN_TIMES = 1000000
TRAINING_BEGIN_TIME = ["2022-08-14","2022-08-15"
,"2022-08-18","2022-08-19","2022-08-20","2022-08-21","2022-08-22","2022-08-25","2022-08-26","2022-08-27","2022-08-28"]
# The algorithms require a vectorized environment to run


def make_env(rank, seed=0):
    """
    Utility function for multiprocessed env.

    :param env_id: (str) the environment ID
    :param num_env: (int) the number of environments you wish to have in subprocesses
    :param seed: (int) the inital seed for RNG
    :param rank: (int) index of the subprocess
    """
    def _init():
        env = Monitor(TrainingEnv(TRAINING_BEGIN_TIME), MODEL_PATH)
        env.seed(seed + rank)
        return env
    set_random_seed(seed)
    return _init


def optimize_params():

    policy = dict(
        activation_fn=th.nn.ReLU,
        net_arch=[
        944,
        868,
        460,
        425,
        317,
        969,
        222,
        401
        ]
    ) 
    return {
        'gamma':0.9633094829575505,
        'learning_rate':9.326741244009415e-05,
        'policy_kwargs':policy
    }


if __name__ == '__main__':
    
    num_cpu = 1  # Number of processes to use
    # Create the vectorized environment

    #env = DummyVecEnv([lambda: Monitor(TrainingEnv(TRAINING_BEGIN_TIME), MODEL_PATH)])

    env = SubprocVecEnv([make_env(i) for i in range(num_cpu)])
    
    model_params = optimize_params()
    actions = env.action_space.shape[-1]
    action_noise = NormalActionNoise(mean=np.zeros(actions), sigma=0.1 * np.ones(actions))

    model = TD3('MlpPolicy', env,verbose=1,action_noise=action_noise,tensorboard_log=TB_LOG_PATH,**model_params)
   
    model.learn(total_timesteps=LEARN_TIMES,callback=SaveModelCallback(check_freq=4096, path=MODEL_PATH,env=env))

    mean_reward, std_reward = evaluate_policy(model, env)
    
    print(f"{mean_reward} {std_reward}")

    model.save("td3_stock")
  
