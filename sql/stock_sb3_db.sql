/*
 Navicat MySQL Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80029
 Source Host           : localhost:3306
 Source Schema         : stock_sb3_db

 Target Server Type    : MySQL
 Target Server Version : 80029
 File Encoding         : 65001

 Date: 22/06/2022 19:11:19
*/
CREATE DATABASE `stock_sb3_db` CHARACTER SET 'utf8mb4';

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for predict_info
-- ----------------------------
DROP TABLE IF EXISTS `predict_info`;
CREATE TABLE `predict_info`  (
  `code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `date` date NOT NULL,
  `buy_price` decimal(10, 4) NULL DEFAULT NULL,
  `sell_price` decimal(10, 4) NULL DEFAULT NULL,
  `expected` decimal(6, 4) NULL DEFAULT NULL,
  PRIMARY KEY (`code`, `date`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for stock_daily_info
-- ----------------------------
DROP TABLE IF EXISTS `stock_daily_info`;
CREATE TABLE `stock_daily_info`  (
  `code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `date` date NOT NULL,
  `open` decimal(16, 8) NULL DEFAULT NULL,
  `high` decimal(16, 8) NULL DEFAULT NULL,
  `low` decimal(16, 8) NULL DEFAULT NULL,
  `close` decimal(16, 8) NULL DEFAULT NULL,
  `stardand` decimal(16, 8) NULL DEFAULT NULL,
  `deal` decimal(16, 8) NULL DEFAULT NULL,
  `turn` decimal(10, 8) NULL DEFAULT NULL,
  `is_st` int NULL DEFAULT NULL,
  PRIMARY KEY (`code`, `date`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for stock_info
-- ----------------------------
DROP TABLE IF EXISTS `stock_info`;
CREATE TABLE `stock_info`  (
  `code` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
