from ctpbee import CtpBee
from runtime import CTA
from stable_baselines3 import SAC

if __name__ == '__main__':
    app = CtpBee('ctp', __name__)

    info = {
        "CONNECT_INFO": {
            "userid": "xxxx",
            "password": "xxxx",
            "brokerid": "xxxx",
           
            "md_address": "tcp://112.65.158.2:51217",
            "td_address": "tcp://112.65.158.2:51209",
            "appid": "client_xxxx_0.9.4",
            "auth_code": "0000000000000000",
            "product_info": ""
        },
        "INTERFACE": "ctp_se",
        "TD_FUNC": True,  # Open trading feature
        "XMIN":[1]
    }
    app.config.from_mapping(info)  # loading config from dict object
    #model = SAC.load("./best_model")
    cta = CTA("cta",None,"rb2301.SHFE")

    app.add_extension(cta)
    app.start() 