import numpy as np
from  MyTT import *
from define import *

MAX_MONEY = 10000000

MAX_VOLUME = 10000

MACD_LIMIT = 100

KDJ_LIMIT = 100

CCI_LIMIT = 100

WR_LIMIT = 100

PRY_LIMIT = 100

BRAR_LIMIT = 100

def get_obs(tick_data,k_list,holder_data):
        
    obs = np.zeros(shape=(OBS_COUNT),dtype=np.float32)
    if len(k_list)<K_LIST_LEN or tick_data is None:
        return obs  
    offset = 0
    close_series = []
    high_series = []
    low_series = []
    open_series = []
    max_price = np.float32(tick_data["price"]*2)
    for i in range(len(k_list)):
        k_item = k_list[i]
        if i >= len(k_list)-2 :
            obs[offset]=k_item["open"]/max_price
            obs[offset+1]=k_item["close"]/max_price
            obs[offset+2]=k_item["high"]/max_price
            obs[offset+3]=k_item["low"]/max_price
            offset+=4
        close_series.append(np.float32(k_item["close"]))
        high_series.append(np.float32(k_item["high"]))
        low_series.append(np.float32(k_item["low"]))
        open_series.append(np.float32(k_item["open"]))
    
    obs[offset]=np.float32(tick_data["price"])/max_price
    offset += 1 
    open_series = pd.Series(open_series, copy=False)
    close_series = pd.Series(close_series, copy=False)
    high_series = pd.Series(high_series, copy=False)
    low_series = pd.Series(low_series, copy=False)
    
    obs[offset+0]=np.float32(tick_data["buy_price_1"])/max_price
    obs[offset+1]=np.float32(tick_data["buy_price_2"])/max_price
    obs[offset+2]=np.float32(tick_data["buy_price_3"])/max_price
    obs[offset+3]=np.float32(tick_data["buy_price_4"])/max_price
    obs[offset+4]=np.float32(tick_data["buy_price_5"])/max_price
    obs[offset+5]=np.float32(tick_data["sell_price_1"])/max_price
    obs[offset+6]=np.float32(tick_data["sell_price_2"])/max_price
    obs[offset+7]=np.float32(tick_data["sell_price_3"])/max_price
    obs[offset+8]=np.float32(tick_data["sell_price_4"])/max_price
    obs[offset+9]=np.float32(tick_data["sell_price_5"])/max_price
    obs[offset+10]=np.float32(tick_data["buy_volume_1"])/MAX_VOLUME
    obs[offset+11]=np.float32(tick_data["buy_volume_2"])/MAX_VOLUME
    obs[offset+12]=np.float32(tick_data["buy_volume_3"])/MAX_VOLUME
    obs[offset+13]=np.float32(tick_data["buy_volume_4"])/MAX_VOLUME
    obs[offset+14]=np.float32(tick_data["buy_volume_5"])/MAX_VOLUME
    obs[offset+15]=np.float32(tick_data["sell_volume_1"])/MAX_VOLUME
    obs[offset+16]=np.float32(tick_data["sell_volume_2"])/MAX_VOLUME
    obs[offset+17]=np.float32(tick_data["sell_volume_3"])/MAX_VOLUME
    obs[offset+18]=np.float32(tick_data["sell_volume_4"])/MAX_VOLUME
    obs[offset+19]=np.float32(tick_data["sell_volume_5"])/MAX_VOLUME

    offset+=20
    k,d,j = KDJ(close_series,high_series,low_series)
    if CROSS(k,d)[-1] == 1 :
        obs[offset+0] = 1
    elif CROSS(d,k)[-1] == 1:
        obs[offset+0] = 0
    else:
        obs[offset+0] = 0.5
    dif,dea,macd = MACD(close_series)
    obs[offset+1]=macd[-1]/MACD_LIMIT+0.5
    if CROSS(dif,dea)[-1] == 1 :
        obs[offset+2] = 1
    elif CROSS(dea,dif)[-1] == 1:
        obs[offset+2] = 0
    else:
        obs[offset+2] = 0.5
    
    upper,mid,lower = BOLL(close_series)
    obs[offset+3]=upper[-1]/max_price
    obs[offset+4]=lower[-1]/max_price
    obs[offset+5]=mid[-1]/max_price
    ma_5 = MA(close_series,5)
    obs[offset+6]=ma_5[-1]/max_price
    ma_10 = MA(close_series,10)
    obs[offset+7]=ma_10[-1]/max_price
    ma_20 = MA(close_series,20)
    obs[offset+8]=ma_20[-1]/max_price
    cci = CCI(close_series,high_series,low_series)
    if cci.values[-1] > CCI_LIMIT :
        obs[offset+9] = 1 
    elif cci.values[-1] < -CCI_LIMIT :
        obs[offset+9] = 0
    else:
        obs[offset+9] = 0.5 
    wr,wr1 = WR(close_series,high_series,low_series)
    obs[offset+10] = wr.values[-1]/WR_LIMIT
    obs[offset+11] = wr1.values[-1]/WR_LIMIT
    pry,mapry = PSY(close_series)
    obs[offset+12] = mapry[-1]/PRY_LIMIT
    obs[offset+13] = pry[-1]/PRY_LIMIT
    bbi = BBI(close_series)
    obs[offset+14] = bbi[-1]/max_price
    offset += 15

    obs[offset+0]=holder_data["buy_order"]/MAX_ORDER
    obs[offset+1]=holder_data["sell_order"]/MAX_ORDER
    usable_order = MAX_ORDER - holder_data["buy_order"] - holder_data["sell_order"]
    obs[offset+2]=usable_order/MAX_ORDER
    return obs