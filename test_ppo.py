
from stable_baselines3 import PPO
from training_env import TrainingEnv

TEST_BEGIN_TIME = ["2022-08-22"]
env = TrainingEnv(TEST_BEGIN_TIME)
model = PPO.load("./best_model",env)

obs = env.reset()
while True:
    action, _ = model.predict(obs,deterministic=True)
    obs, rewards, dones, info = env.step(action)
    env.render()
    if dones :
        env.close()
        break